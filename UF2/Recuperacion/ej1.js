// Exercici 1: 
// Definir un array de persones (amb 10 elements), que tingui com a elements un objecte de
// tipus {nom:"",cognom:"", data_naixement:"",salari:""} 
// Defineix un algorisme del mètode sort() per ordenar per salari.
// Redefineix un algorisme del mètode sort() per ordenar per data_naixement 

var p = [
    {nom:"Pedro", cognom:"Torres", data_naixement:"18-02-1991", salari:1272.57},
    {nom:"Miguel", cognom:"Chaves", data_naixement:"04-10-1997", salari:1443.08},
    {nom:"Vanessa", cognom:"Barranco", data_naixement:"11-04-2000", salari:1078.13},
    {nom:"Víctor", cognom:"Barragan", data_naixement:"25-04-1999", salari:1079.19},
    {nom:"Angustias", cognom:"Vives", data_naixement:"16-04-2000", salari:1426.07},
    {nom:"Leandro", cognom:"Diego", data_naixement:"14-05-1992", salari:1120.17},
    {nom:"Avelino", cognom:"Arenas", data_naixement:"02-02-1993", salari:1499.76},
    {nom:"Milagros", cognom:"Nogales", data_naixement:"07-10-1995", salari:1456.49},
    {nom:"Jessica", cognom:"Asensio", data_naixement:"06-09-1999", salari:1285.24},
    {nom:"Anne", cognom:"Coll", data_naixement:"07-08-1997", salari:1091.03}
];

function ordenarSalari(personas) {
    var ordenarPrice = personas.sort((p1,p2)=>(p1.salari > p2.salari) ?1:(p1.salari < p2.salari)?-1:0);
    console.log(ordenarPrice);
}

ordenarSalari(p);