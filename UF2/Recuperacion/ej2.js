// Exercici 2: Donada una llista de noms en el format cognoms, nom, per exemple: Garcia Pijoan, Maria. 
// Construir una llista pseudo-aleatòria de més de 50 valors (>50) posar-los en forma de matriu (array).
// Definir una funció d'ordenació de manera que la llista-array quedi ordenada pel nom.
// Un cop trobem la funció, definir amb prototype el mètode ordenar_per_nom()

var nombres = ["Monzon Amoros, Luis", "Manuel Ojeda, Jose", "Torres Cuellar, Daniel","Maria Carrasco, Laura",
"Rial Lara, Sandra","Valiente Tapia, Carlos", "Rodrigo Barreiro, Mariano","Serna Vargas, Aya",
"Socorro Guardiola, Maria"];

var orden = [];
var prueba = [];

var espacio = ' ';
var count = 0;


for (var i = 0; i < nombres.length; i++) {
    orden.push(nombres[i].replace(', ', ' '));
}

for (var index = 0; index < orden.length; index++) {
    var matriz = orden[index];
    var matriz = matriz.split(' ').reverse().join(' ');
    prueba.push(matriz);
}

prueba.sort();
for (var a = 0; a < prueba.length; a++) {
    var matriz = prueba[a];
    var matriz = matriz.split(' ').reverse().join(' ');
    prueba[a]=matriz;
}

for (var b = 0; b < prueba.length; b++) {
    document.write(prueba[b]+'<br>');
}