/*
1. Re-create challenge 1, but this time using an ES6 class;
2. Add a getter called 'speedUS' which returns the current speed in mi/h (divide by 1.6);
3. Add a setter called 'speedUS' which sets the current speed in mi/h (but converts it to km/h before storing the value, by multiplying the input by 1.6);
4. Create a new car and experiment with the accelerate and brake methods, and with the getter and setter.
*/


// Se crea la clase
class Car {
    // Se crea el constructor que son los metodos de la clase
    constructor (make, speed){
        this.make = make;
        this.speed = speed;
    }
    // Se crea un metodo de acceleracion
    accelerate(){
        console.log(this.speed += 10);
    }
    // Otro de frenado
    brake(){
        console.log(this.speed -= 5);
    }
    // Se crea un get que nos regresara el valor
    get speedUS(){
        return this.speed / 1.6;
    }

    // Se crea el set, con el valor dado para la operacion
    set speedUS(speed){
        this.speed = speed;
        
    }
}



// Se crea el objeto
const bmw = new Car('BMW',120);
// Se muestra el valor
console.log(bmw);
// Se llama a la operacion
console.log(bmw.speedUS);
// Se da un valor
bmw.speedUS = 100;
// Y se vuelve a llamar con el nuevo valor
console.log(bmw.speedUS);

const ford = new Car('FORD', 120);
console.log(ford);
ford.accelerate();
ford.brake();
console.log(ford.speedUS);
ford.speedUS = 79;
console.log(ford.speedUS);