/*
1. Create one player array for each team (variables 'players1' and 'players2')
2. The first player in any player array is the goalkeeper and the others are field players. For Bayern Munich (team 1) create one variable ('gk') with the goalkeeper's name, and one array ('fieldPlayers') with all the remaining 10 field players
3. Create an array 'allPlayers' containing all players of both teams (22 players)
4. During the game, Bayern Munich (team 1) used 3 substitute players. So create a new array ('players1Final') containing all the original team1 players plus 'Thiago', 'Coutinho' and 'Perisic'
5. Based on the game.odds object, create one variable for each odd (called 'team1', 'draw' and 'team2')
6. Write a function ('printGoals') that receives an arbitrary number of player names (NOT an array) and prints each of them to the console, along with the number of goals that were scored in total (number of player names passed in)
7. The team with the lower odd is more likely to win. Print to the console which team is more likely to win, WITHOUT using an if/else statement or the ternary operator.
TEST DATA FOR 6: Use players 'Davies', 'Muller', 'Lewandowski' and 'Kimmich'. Then, call the function again with players from game.scored

1. Crear una matriz de jugadores para cada equipo (variables 'jugadores1' y 'jugadores2')
2. El primer jugador de cualquier grupo de jugadores es el portero y los demás son jugadores de campo. Para el Bayern Munich (equipo 1) crear una variable ('gk') con el nombre del portero, y una matriz ('fieldPlayers') con los 10 jugadores de campo restantes
3. Crear una matriz 'allPlayers' que contenga todos los jugadores de ambos equipos (22 jugadores)
4. Durante el juego, el Bayern Munich (equipo 1) utilizó 3 jugadores suplentes. Así que creó un nuevo conjunto ('jugadores1Final') que contenía todos los jugadores del equipo original1 más 'Thiago', 'Coutinho' y 'Perisic'.
5. Basado en el objeto game.odds, crea una variable para cada impar (llamado "equipo1", "empate" y "equipo2")
6. Escriba una función ('printGoals') que reciba un número arbitrario de nombres de jugadores (NO una matriz) e imprima cada uno de ellos en la consola, junto con el número de goles que se marcaron en total (número de nombres de jugadores pasados)
7. El equipo con el impar inferior tiene más probabilidades de ganar. Imprime en la consola qué equipo tiene más probabilidades de ganar, SIN usar una declaración de "si/no" o el operador ternario.
DATOS DE PRUEBA PARA 6: Utiliza los jugadores 'Davies', 'Muller', 'Lewandowski' y 'Kimmich'. Luego, llame la función de nuevo con los jugadores de game.scored
*/

const game = {
    team1: 'Bayern Munich',
    team2: 'Borrussia Dortmund',
    players: [
      [
        'Neuer',
        'Pavard',
        'Martinez',
        'Alaba',
        'Davies',
        'Kimmich',
        'Goretzka',
        'Coman',
        'Muller',
        'Gnarby',
        'Lewandowski',
      ],
      [
        'Burki',
        'Schulz',
        'Hummels',
        'Akanji',
        'Hakimi',
        'Weigl',
        'Witsel',
        'Hazard',
        'Brandt',
        'Sancho',
        'Gotze',
      ],
    ],
    score: '4:0',
    scored: ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'],
    date: 'Nov 9th, 2037',
    odds: {
      team1: 1.33,
      x: 3.25,
      team2: 6.5,
    },
};
console.log('1');
const [players1, players2] = game.players;
console.log(players1, players2);
  
console.log('2');
const [goalkeeper, ...fieldPlayers] = players1;
console.log(goalkeeper, fieldPlayers);

console.log('3');
const allPlayers = [...players1, ...players2];
console.log(allPlayers);

console.log('4');
const players1Final = [...players1, 'Thiago', 'Coutinho', 'Perisic'];
console.log(players1Final);

console.log('5');
const {
    odds : {team1, x:draw, team2}
}=game;
console.log(team1, draw, team2);

console.log('6');
const printGoals = function(...players){
    console.log(players);
    console.log(`${players.lenght}`);
};

printGoals(...game.scored);
team1 < team2 && console.log('Team 1 tiene mas posibilidad de ganar');
team1 > team2 && console.log('Team 2 tiene mas posibilidad de ganar');