const parrafo=["I will here give a brief sketch of the progress of opinion on the Origin of Species."
+"Untilrecently the great majority of naturalists believed that species were immutableproductions, and" 
+"had been separately created. This view has been ably maintained bymany authors. Some few naturalists," 
+"on the other hand, have believed that speciesundergo modification, and that the existing forms of life" 
+"are the descendants by truegeneration of pre existing forms. Passing over allusions to the subject in" 
+"the classicalwriters (Aristotle, in his \"Physicae Auscultationes\" (lib.2, cap.8, s.2), after remarking" 
+"thatrain does not fall in order to make the corn grow, any more than it falls to spoil the farmer'scorn" 
+"when threshed out of doors, applies the same argument to organisation; and adds(as translated by Mr. Clair Grece," 
+"who first pointed out the passage to me), \"So whathinders the different parts (of the body) from having this merely" 
+"accidental relation innature? as the teeth, for example, grow by necessity, the front ones sharp, adapted fordividing," 
+"and the grinders flat, and serviceable for masticating the food; since they werenot made for the sake of this, but it" 
+"was the result of accident. And in like manner as toother parts in which there appears to exist an adaptation to an end." 
+"Wheresoever,therefore, all things together (that is all the parts of one whole) happened like as if theywere made for the" 
+"sake of something, these were preserved, having been appropriatelyconstituted by an internal spontaneity; and whatsoever" 
+"things were not thus constituted,perished and still perish. \"We here see the principle of natural selection shadowed" 
+"forth,but how little Aristotle fully comprehended the principle, is shown by his remarks on theformation of the" 
+"teeth.), the first author who in modern times has treated it in a scientificspirit was Buffon. But as his opinions" 
+"fluctuated greatly at different periods, and as hedoes not enter on the causes or means of the transformation of" 
+"species, I need not hereenter on details."]
//document.write(parrafo);
const letras =new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
    'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'W', 'X', 'Y', 'Z');
const palabras=new Array(letras);

function inicia2() {
    //document.write(palabras);
    letras.forEach(function(letras) {
        document.write(`${letras}`);
        letras[`${letras}`]=new Array(parrafo)
    });
}


inicia2();
//recorrerFrase2(parrafo);