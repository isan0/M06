/*
1. Usar una función de constructor para implementar un coche. Un coche tiene una marca y una propiedad de velocidad. La propiedad de velocidad es la velocidad actual del coche en km/h;
2. 2. Implementar un método de "aceleración" que aumente la velocidad del coche en 10, y registrar la nueva velocidad en la consola;
3. 3. Implementar un método de "freno" que disminuirá la velocidad del auto en 5, y registrar la nueva velocidad en la consola;
4. Crear 2 objetos para el coche y experimentar con el llamado "acelerar" y "frenar" varias veces en cada uno de ellos.

COCHE DE DATOS 1: "BMW" yendo a 120 km/h
COCHE DE DATOS 2: 'Mercedes' yendo a 95 km/h

*/


// Se crea una funcion de constructor
const Car = function(make, speed){
    // Creamos las propiedades
    this.make = make;
    this.speed = speed;
};


// Se crea los constructores
Car.prototype.accelerate = function(){
    console.log(this.speed += 10);
};

Car.prototype.brake = function() {
    console.log(this.speed -= 5);
};

// Se crea los objetos
const bmw = new Car('BMW', 120);
const mercedes = new Car('Mercedes', 95);

// Se llama a los constructores
bmw.accelerate();
bmw.accelerate();
bmw.brake();
bmw.accelerate();