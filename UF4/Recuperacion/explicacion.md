## Explicacion
### Referenciant el projecte del Marketplace que hem fet a classe durant la UF4, defineix els següent conceptes:


**Comunicació assíncrona**  
La comunicación consiste cuando estamos navegando en una pagina web permitiendo de la accion que uno realice no se tenga que volver a recargar la pagina entera si no solo la parte del codigo sin recargar la pagina.  
Lo usamos para que la pagina valla mas rapido, se implemento en la vista de los productos  

**AJAX, AXIOS**  
Esta tecnologia permite que los contenido de la pagina se actualize sin la necesidad de que la pagina se actualice, realiza peticiones en segundo plano haciendo que la aplicacion no se bloqueé mientras realiza una petición.  
Se usa para actualizar parte del codigo, se implemente para la manipulacion de creacion, edicion, eliminacion y actualizacion de los productos


**REST**  
Es un estilo de arquitectura que no se aplica ningun estandar a si mismo.  
Se usa REST para manipulacion de los productos, se uso para añadir los metodos de creacion, edicion, eliminacion de los productos  

**JSON**  
El JSON (Javascript Object Notation), utiliza la propiedad responseText que hace que los datos pasado por un fichero los convierta en objecto de Javascript como cualquiera para poder utilizarlos.  
Usamos JSON en el proyecto para obtener datos, se implementa cada vez que se quiera añadir un nuevo producto.  

**Vue**  
Es un framework Javascript para desarrollar aplicaciones front de manera sencilla y comoda.  
El uso de vue nos permite la creacion de proyecto con su alta compatibilidad con Javascript, se implementa desde la creacion de cada nuevo proyecto.  

**SPA**  
Las SPA (Single Page Application), es una pagina web en donde carga solo un fichero HTML y todo su contenido se muestra en una sola pagina, es decir que al iniciar la pagina tanto codigo HTML, CSS y Javascript se carga, las SPA usa el metodo de vista que son los apartados de la pagina.  
Se uso SPA para las vistas, se implementa para vada vista como el MarketPlace, novedades, Carrito

**Routing**  
El rotuting/rutas nos permite decidir para una url determinada que componente manejara y gestiona el viaje del usuario por la pagina.  
Usando routing en el proyecto podemos ir a otra pagina sin problemas, se implementa al momento de crear una nueva vista.