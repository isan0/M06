// Exercici 1: Donada una cadena, fes servir indexOf i lastIndexOf per cercar la posició primera o última
// d'una cadena dins d'una altra. Si vols saber totes les ocurrències. Fes servir bucles.

var frase = 'Esta es la frase que se usara para el ejemplo';

document.write(frase.indexOf('frase')+'<br>');
document.write(frase.lastIndexOf('usara'));