// 12. Demanar 10 num. i treure per pantalla el promig del 3, 4, 5, 6 i 7

function promedio() {
    var total = 0;
    var count = 0;
    for (var i = 1; i <= 10; i++) {
        var num = parseInt(prompt('Ingresa el numero: '))
        if (i >= 3 && i <= 7) {
            count++;
            total += num;
        }
    }
    total /= count;
    document.write('El promedio de los numeros del 3 al 7 es: ' + total);
}

promedio();