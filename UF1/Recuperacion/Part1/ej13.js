// 13. Demanar per teclat:
// - Les edats de 5 estudiants del torn de matí
// - Les edats de 6 estudiants del torn de tarda
// - Les edats de 11 estudiants del torn de nit
// S'ha d'obtenir la mitjana de les edats de cada torn, hi haurà 3 mitjanes, i mostrar-les en la pàgina html.
// També s'ha de mostrar els torns en funció de la mitjana de la més alta a la més baixa

function jornada() {
    var mati = 0;
    var tarda = 0;
    var nit = 0;
    var count = 0;
    var media = [];
    var alto = 0;

    for (var i = 1; i <= 5; i++) {
        var edad = parseInt(prompt('Ingresa la edad del turno de la mañana: '));
        mati += edad;
        count = i;
    }
    mati /= count;
    mati = Math.trunc(mati);
    media.push(mati);

    for (var i = 1; i <= 6; i++) {
        var edad = parseInt(prompt('Ingresa la edad del turno de la tarda: '));
        tarda += edad;
        count = i;
    }
    tarda /= count;
    tarda = Math.trunc(tarda);
    media.push(tarda);

    for (var i =  1; i <= 11; i++) {
        var edad =  parseInt(prompt('Ingresa la edad del turno de la noche: '));
        nit += edad;
        count = i;
    }
    nit /= count;
    nit = Math.trunc(nit);
    media.push(nit);

    alto = media.sort((a,b) => b-a);

    document.write('La media del turno de la mañana es: ' + mati);
    document.write('<br>La media del turno de la tarde es: ' + tarda);
    document.write('<br>La media del turno de la noche es: ' + nit);
    document.write('<br>El orden de la media desde la mas alta a la menor es: ' + media.sort((a,b) => b-a));
}

jornada();