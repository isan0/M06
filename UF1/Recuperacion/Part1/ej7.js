// 7. LLegir per teclat 2 números. Si el 1r és major que el 2n informar de la seva suma i de la seva diferència (restar), en cas contrari informar del producte i la divisió (el 2n número serà el divisor).

function diferencia() {
    var n1 = parseInt(prompt('Ingresa el primer numero: '));
    var n2 = parseInt(prompt('Ingresa el segundo numero: '));
    if (n1 > n2) {
        var s = n1 + n2;
        document.write('La suma de los 2 numeros es: ' + s);
        var r = n1 - n2;
        document.write('<br>La diferencia entre los 2 numeros es: ' + r);
    }
    else {
        var m = n2 * n1;
        document.write('El producto entre los 2 numeros es: ' + m);
        var d = n1 / n2;
        document.write('<br>La division de los 2 numeros es: ' + d);
    }
}

diferencia();