Versiones de Javascript
1- ECMAScript 1 (1997): primera edicion.
2- ECMAScript 2 (1998): solo se realizaron cambios editoriales.
3- ECMAScript 3 (1999): se añadieron expresiones regulares y prueba/tratamiento.
4- ECMAScript 4: sin publicar.
5- ECMAScript 5 (2009): se añadio el modo estricto, el soporte JSON, String.trim(),
Array.isArray() y metodo de iteracion de arrays.
5.1- ECMAScript 5.1 (2011): cambios de editoriales.
6- ECMAScript 2015: se añade let y const, valores de parametros por defecto, Array.find(),
Array.find(), Array.findIndex().
7- ECMAScript 2016: se añadio operador exponencial (**).
8- ECMAScript 2017: se añade el relleno de cadenas, nuevas propiedades de los objetos,
funciones de Async y memoria compartida
9- ECMAScript 2018: se añade propiedades de reposo/dispersion, iteracion asincrona,
Promise.finally(), RegExp